package tests.Login;


import java.util.concurrent.TimeUnit;

import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Gauge;
import driver.Driver;
import pages.Login.Login;

import org.openqa.selenium.By;
import java.io.BufferedWriter;		
import java.io.File;		
import java.io.FileWriter;
import org.openqa.selenium.Cookie;	

public class LoginTest {
    
   public static Login loginPage = new Login(Driver.webDriver);

    @Step("Goto anexia internal-engine page")
    public void gotoANXEnginePage() throws InterruptedException {

        
       // String app_url = System.getenv("APP_URL");
        Driver.webDriver.get("https://integration.ps.anexia-it.com/login");
        final File file = new File("cookies.data");

        try{
                file.delete();
                file.createNewFile();
                
                final FileWriter fileWrite = new FileWriter(file);
                final BufferedWriter Bwrite = new BufferedWriter(fileWrite);

                for(final Cookie ck : Driver.webDriver.manage().getCookies()){
                    Bwrite.write((ck.getName()+";"+ck.getValue()+";"+ck.getDomain()+";"+ck.getPath()+";"+ck.getExpiry()+";"+ck.isSecure()));
                    Bwrite.newLine();   
                }
            
                Bwrite.close();			
                fileWrite.close();

        }catch(final Exception ex){
            ex.printStackTrace();
        }
        
    }

    @Step("Enter valid username")
    public void enterValidUsername() throws InterruptedException {
        loginPage.enterUsername("JaneDoe@anexia-it.com");   
    }

    @Step("Click on Continue")
    public void clickContinue() {
        loginPage.clickSubmitBtn();
    }   

    @Step("Enter valid password")
    public void EnterValidPassword() {
        loginPage.enterPassword("testUser09@2020");
    }

    @Step("Click on Login")
    public void clickLogin() {
        loginPage.clickSubmitBtn();
    }

    @Step("Verify if login was successful")
    public void verifySuccessfulLogin() {

        Driver.webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

       final String username =  Driver.webDriver.findElement(By.xpath("/html/body/div[1]/div[3]/div[1]/div[1]/div[2]/ul/li[4]/a/span")).getText();
        //assertThat(Driver.webDriver.getTitle()).contains("Gauge");
        
        if(username.equals("Jane Doe")){
            System.out.println("Login was successful");
            Gauge.writeMessage("Login was successful");

        }
        else {
            System.out.println("Login was not successful");
            Gauge.writeMessage("Login was not successful");
        }

         
    }

    @Step("Activate checkbox to stay signed in")
    public void clickStaySignedInCheckbox() {
        loginPage.staysignedin();
       // Driver.webDriver.findElement(By.xpath("/html/body/div[1]/div[1]/div/div/form/div[5]/div/div/div[1]/label/div")).click();
    }

    @Step("Enter invalid username")
    public void enterInValidUsername() throws InterruptedException {
       loginPage.enterUsername("JaneDoe@");
        
    }

    @Step("Verify text of errorMsg")
    public void verifyTextofErrorMsg() throws InterruptedException {
        final String errorMsg = Driver.webDriver.findElement(By.xpath("/html/body/div[6]/div")).getText();
        final String ANXerrorMsg = "Username and/or password invalid.";

        if(!errorMsg.equals(ANXerrorMsg)){
           Gauge.writeMessage(errorMsg);
        }        
    }

}
