package tests.Organization.Account.Users_Teams;

import com.thoughtworks.gauge.Step;
import org.openqa.selenium.By;
import driver.Driver;
import tests.BasicTestImplementation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.StringTokenizer;
import org.openqa.selenium.Cookie;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import org.openqa.selenium.support.ui.WebDriverWait;

public class UserTeamsTest {
    String searchInput = "";
    String rowID = "";
    String guid_Anexia = "bd4642b084aa4631beb36e3130877a88";

    public  WebDriverWait wait = new WebDriverWait(Driver.webDriver, 30);

    public BasicTestImplementation basics = new BasicTestImplementation();

    @Step("Open integration page with stored cookies")
    public void openProfile() throws InterruptedException {

        try {
            File file = new File("Cookies.data");
            FileReader fileReader = new FileReader(file);
            BufferedReader Buffreader = new BufferedReader(fileReader);
            String strline;

            while ((strline = Buffreader.readLine()) != null) {
                StringTokenizer token = new StringTokenizer(strline, ";");
                while (token.hasMoreTokens()) {
                    String name = token.nextToken();
                    String value = token.nextToken();
                    String domain = token.nextToken();
                    String path = token.nextToken();
                    Date expiry = null;
                    String dt;

                    if (!(dt = token.nextToken()).equals("null")) {

                        expiry = new SimpleDateFormat("EEE MMM d HH:mm:ss zzz uuuu", Locale.US).parse(dt);
                        System.out.println("expiry = " + expiry);
                    }

                    Boolean isSecure = Boolean.valueOf(token.nextToken()).booleanValue();
                    Cookie ck = new Cookie(name, value, domain, path, expiry, isSecure);
                    System.out.println("--------------------------------" + ck);
                    // String app_url = System.getenv("APP_URL");

                    // Cookie testCookie = new Cookie("PHPSESSION",
                    // "410d1066e7ab2172d044e30bc26f9444");

                    Driver.webDriver.get("https://integration.ps.anexia-it.com/login");
                    Driver.webDriver.manage().deleteAllCookies();
                    Driver.webDriver.manage().addCookie(ck); // This will add the stored cookie to your current session
                    Driver.webDriver.get("https://integration.ps.anexia-it.com/login");
                    // Driver.webDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Step("Click on username")
    public void clickOnUsername() throws InterruptedException {
        Driver.webDriver.findElement(By.xpath("/html/body/div[1]/div[3]/div[1]/div[1]/div[2]/ul/li[4]/a")).click();
    }

    @Step("Click on logout")
    public void clickLogout() throws InterruptedException {

        WebElement element = Driver.webDriver
                .findElement(By.xpath("/html/body/div[1]/div[3]/div[1]/div[1]/div[2]/ul/li[4]/ul/li[10]/a"));

        WebDriverWait wait = new WebDriverWait(Driver.webDriver, 2);

        wait.until(ExpectedConditions.visibilityOf(element));
        element.click();

        // JavascriptExecutor js = (JavascriptExecutor)Driver.webDriver;
        // js.executeScript("arguments[0].click();", element);

        // Driver.webDriver.findElement(By.xpath("//*[@id='anx-user-menu']/ul/li[4]/ul/li[10]/a")).click();
    }

    @Step("Verify LogoutMsg")
    public void verifyLogoutMsg() throws InterruptedException {

        String msg = Driver.webDriver.findElement(By.xpath("/html/body/div[6]/div")).getText();

        if (msg.equals("You have been logged out.") || msg.equals("Sie wurden abgemeldet")) {
            System.out.println("Logout was successfull");
        } else {
            System.out.println("Something went wrong during the logout process");
        }
    }

    @Step("Click on Account within ANEXIA scope")
    public void clickOnANEXIA() throws InterruptedException {

        //Driver.webDriver.manage().window().maximize();

        //Driver.webDriver.findElement(By.xpath("//*[@id='anx-modules']/div[2]/a")).click();
        //Driver.webDriver.findElement(By.xpath("//*[@id='anx-modules']/div[2]/div/ul/li[1]/a")).click();

        Driver.webDriver.get("https://integration.ps.anexia-it.com/org/483490945c424add88da6f97ffd2fb13/edit");

    }

    @Step("Click on Users&Teams")
    public void clickOnUsersTeams() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(Driver.webDriver, 2);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='anx-page-navigation']/ul/li[4]/a")));

        Driver.webDriver.findElement(By.xpath("//*[@id='anx-page-navigation']/ul/li[4]/a")).click();
    }

    @Step("Click on Add User")
    public void clickOnAddUser() throws InterruptedException {

        WebDriverWait wait = new WebDriverWait(Driver.webDriver, 2);
        wait.until(ExpectedConditions
                .elementToBeClickable(By.xpath("//*[@id='dt-users_wrapper']/div/div[2]/div[2]/a[2]")));

        WebElement add_btn = Driver.webDriver
                .findElement(By.xpath("//*[@id='dt-users_wrapper']/div/div[2]/div[2]/a[2]"));

        if (add_btn != null) {
            System.out.println("Element is present");
            add_btn.click();
        } else {
            System.out.println("Element is absent");
        }
    }

    @Step("Enter data within the modal")
    public void addDatatoModal() throws InterruptedException {


        WebDriverWait wait = new WebDriverWait(Driver.webDriver, 2);
        wait.until(
                ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='unimodal']/div/div")));

        // --------------------------------USER
        // INFORMATION--------------------------------
        WebElement email_r = Driver.webDriver.findElement(By.xpath("//*[@id='formEmail']"));
        email_r.sendKeys("AcceptanceTesting@anexia-it.com");
        WebElement firstname = Driver.webDriver.findElement(By.xpath("//*[@id='formFirstName']"));
        firstname.sendKeys("Acceptance");
        WebElement lastname_r = Driver.webDriver.findElement(By.xpath("//*[@id='formLastName']"));
        lastname_r.sendKeys("Testing");
        // Select drpLanguage = new
        // Select(Driver.webDriver.findElement(By.xpath("//*[@id='s2id_formLanguage']/a")));
        // drpLanguage.selectByIndex(1);
        // Select drpPermissionGroups = new
        // Select(Driver.webDriver.findElement(By.xpath("//*[@id='s2id_formPermissionGroups']/ul")));
        // drpPermissionGroups.selectByVisibleText("D17 - Platform Solutions");
        // select teams

        Driver.webDriver.findElement(By.xpath("//*[@id='unimodal']/div/div/div[3]/button[2]")).click();
        // --------------------------------USER
        // INFORMATION--------------------------------

        // --------------------------------ADDITIONAL
        // INFORMATION--------------------------------
        WebElement position = Driver.webDriver.findElement(By.xpath("//*[@id='formPosition']"));
        position.sendKeys("QA-Engineer");
        WebElement address = Driver.webDriver.findElement(By.xpath("//*[@id='formAddress']"));
        address.clear();
        address.sendKeys("Feldkirchnerstrasse 144");
        //WebElement zip = Driver.webDriver.findElement(By.xpath("//*[@id='formZip']"));
        //WebElement city = Driver.webDriver.findElement(By.xpath("//*[@id='formCity']"));

        WebElement phone = Driver.webDriver.findElement(By.xpath("//*[@id='formPhone']"));
        phone.sendKeys("+43 4243-37798");
        WebElement mobilePhone = Driver.webDriver.findElement(By.xpath("//*[@id='formMobilePhone']"));
        mobilePhone.sendKeys("+43 650/9250018");
        Driver.webDriver.findElement(By.xpath("//*[@id='unimodal']/div/div/div[3]/button[2]")).click();
        // --------------------------------ADDITIONAL
        // INFORMATION--------------------------------

        // --------------------------------PRIVATE
        // INFORMATION--------------------------------
        Driver.webDriver.findElement(By.xpath("//*[@id='unimodal']/div/div/div[3]/button[2]")).click();
        // --------------------------------PRIVATE
        // INFORMATION--------------------------------

    }

    public void searchWithinDT(String xpathDataTable, String xpathsearchbtn, String xpathSearchInput,
            String searchInput) throws InterruptedException {

        WebDriverWait wait_click = new WebDriverWait(Driver.webDriver, 2);
        wait_click.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathsearchbtn)));

        Driver.webDriver.findElement(By.xpath(xpathsearchbtn)).click();
        Driver.webDriver.findElement(By.xpath(xpathSearchInput)).sendKeys(searchInput);

        // not nice solution-but it works! Although, this should be changed!!!->ToDO
        Thread.sleep(1000);

        String celtext = "";

        WebElement mytable = Driver.webDriver.findElement(By.xpath(xpathDataTable));
        List<WebElement> rows_table = mytable.findElements(By.tagName("tr"));

        int rows_count = rows_table.size();

        System.out.println("---------------rows:" + rows_count);

        // Loop will execute till the last row of table.
        for (int row = 0; row < rows_count; row++) {

            rowID = rows_table.get(row).getAttribute("id");
            // To locate columns(cells) of that specific row.
            List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName("td"));
            // To calculate no of columns(cells) In that specific row.
            int columns_count = Columns_row.size();
            //System.out.println("---------------------------------row_id:" + rowID);
            //System.out.println("Number of cells In Row " + row + " are " + columns_count);

            // Loop will execute till the last cell of that specific row.
            for (int column = 0; column < columns_count; column++) {
                // To retrieve text from that specific cell.
                celtext = Columns_row.get(column).getText();

                //System.out
                //        .println("Cell Value Of row number " + row + " and column number " + column + " Is " + celtext);

            }
            //System.out.println("--------------------------------------------------");
        }
        if (celtext.equals("No matching records found") || celtext.equals("Keine Einträge gefunden")) {
            throw new InterruptedException("Added User was not found");
        }
    }

    @Step("Search")
    public void searchForUser() throws InterruptedException {

        // ToDo: verify if Search function works -->speichern, wie viele Elemente vor
        // der Suche in der Tabelle waren
        // und danach vergleichen, wie viele Elemente nach der Suche in der Tabelle
        // aufscheinen
        searchInput = "Acceptance";
        //WebDriverWait wait = new WebDriverWait(Driver.webDriver, 2);
        //wait.until(
        //        ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@id='unimodal']/div/div/div[2]/div[2]")));

        searchWithinDT("//*[@id='dt-users']/tbody", "//*[@id='dt-users_filter']/span/i",
                "//*[@id='dt-users_filter']/div/input", searchInput);
    }

    @Step("Verify ToolTip for DeleteBtn")
    public void verifyToolTip() throws InterruptedException {

        WebElement deleteBtn = Driver.webDriver.findElement(By.xpath("//*[@id='" + rowID + "']/td[7]/a[4]"));
        String tooltiptext = deleteBtn.getAttribute("data-original-title");
        System.out.println("------------------Tooltip text is : " + tooltiptext);

    }

    @Step("Delete User")
    public void deleteUser() throws InterruptedException {

        Driver.webDriver.findElement(By.xpath("//*[@id='" + rowID + "']/td[7]/a[4]")).click();

        WebDriverWait modal = new WebDriverWait(Driver.webDriver, 5);
        modal.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[12]/div/div")));

        Driver.webDriver.findElement(By.xpath("/html/body/div[12]/div/div/div[3]/button[2]")).click();
    }

    @Step("Edit User")
    public void editUser() throws InterruptedException {

        Driver.webDriver.findElement(By.xpath("//*[@id='" + rowID + "']/td[7]/a[2]")).click();

        // To be verify, that the correct page is opened
        WebDriverWait modal = new WebDriverWait(Driver.webDriver, 5);
        modal.until(ExpectedConditions.urlToBe("https://integration.ps.anexia-it.com/user/" + guid_Anexia + "/edit"));

        // Edit Firstname
        WebElement input_firstname = Driver.webDriver.findElement(By.xpath("//*[@id='formFirstName']"));
        input_firstname.clear();
        input_firstname.sendKeys("Acceptance");

        // Edit mobilePhoneNumber
        WebElement mobilePhoneNumber = Driver.webDriver.findElement(By.xpath("//*[@id='formPhone']"));
        mobilePhoneNumber.clear();
        mobilePhoneNumber.sendKeys("+43 650 9250020");

          Driver.webDriver.findElement(By.xpath("//*[@id='submit-all-forms']")).click();
         
    }

    @Step("Verify editation")
    public void verifyEditation() throws InterruptedException {
        WebDriverWait msg_wait = new WebDriverWait(Driver.webDriver, 2);
        WebElement AnxMsg = Driver.webDriver.findElement(By.xpath("//html/body/div[2]"));
        msg_wait.until(ExpectedConditions.visibilityOf(AnxMsg));

        String anxMsg = AnxMsg.getText();

        if (!anxMsg.equals("Successfully updated user")) {
            throw new InterruptedException("Updating user was not successfully");
        }

        if (!anxMsg.equals("User erfolgreich aktualisiert")) {
            throw new InterruptedException("Updating user was not successfully");
        }
    }

    @Step("Go to Teams")
    public void goToTeams() throws InterruptedException {

        Driver.webDriver.get("https://integration.ps.anexia-it.com/org/" + guid_Anexia + "/teams");

        // searchInput = "QA-Team";
        searchInput = "QA-Team";
        searchWithinDT("//*[@id='dt-team']/tbody", "//*[@id='dt-team_filter']/span/i",
                "//*[@id='dt-team_filter']/div/input", searchInput);

        // *[@id="dt-team"]/tbody/tr/td[4]/a[1]
    }

    @Step("Edit Team")
    public void editTeam() throws InterruptedException {

        Driver.webDriver.findElement(By.xpath("//*[@id='dt-team']/tbody/tr/td[4]/a[1]")).click();

        // To be verify, that the correct modal is opened
        WebDriverWait modal = new WebDriverWait(Driver.webDriver, 5);
        modal.until(ExpectedConditions.urlToBe("https://integration.ps.anexia-it.com/org/" + guid_Anexia + "/teams"));

        /*
         * WebElement updateTeamModal =
         * Driver.webDriver.findElement(By.xpath("//*[@id='s2id_users-input']"));
         * WebDriverWait wait_modal = new WebDriverWait(Driver.webDriver, 5);
         * wait_modal.until(ExpectedConditions.visibilityOf(updateTeamModal));
         * 
         * Driver.webDriver.findElement(By.xpath(
         * "//*[@id='s2id_users-input']/ul/li[1]/a")).click();
         */

        Driver.webDriver.findElement(By.xpath("//*[@id='team-submit']")).click();
    }

	@Step("Assign new Permission group")
	public void assignNewPermissionGroup() throws InterruptedException {
        Driver.webDriver.findElement(By.xpath("//*[@id='" + rowID + "']/td[7]/a[2]")).click();

        String id = "s2id_formPermissionGroups";

        //String id = "s2id_formCountry";
        String value = "Ascension Island";
        String number = "3";

        //*[@id="s2id_autogen3"]

            //basics.clearMultipleSelectAll(id);
           // basics.selectMultipleOption(id, number);
           //basics.selectOption(id, value);

           basics.select2Option(id, value);

	}

}