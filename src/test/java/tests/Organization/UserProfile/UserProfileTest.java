package tests.Organization.UserProfile;


import com.thoughtworks.gauge.Step;
import org.openqa.selenium.support.ui.WebDriverWait;
import driver.Driver;
import pages.Organization.UserProfile.UserProfile;

public class UserProfileTest {

     public static UserProfile userProfile = new UserProfile(Driver.webDriver);

     public  WebDriverWait wait = new WebDriverWait(Driver.webDriver, 5);

	@Step("Click on UserProfile")
	public void clickOnUserprofile() throws InterruptedException{
		Driver.webDriver.get("https://integration.ps.anexia-it.com/user/b51ef959791941049d31867c6cc693cc/edit");
	}

	@Step("Change User Data")
	public void changeUserData() throws InterruptedException{
        userProfile.enterFormInputs("Jane", "Doe", "9500", "Villach", "+43 650 9250022");
	}

	@Step("Click Save Button")
	public void clickSaveButton() throws InterruptedException{
		userProfile.saveChanges();
    }
    
  

	@Step("Verify changes")
	public void verifyChanges() throws InterruptedException{

    
        //wait.until(ExpectedConditions.visibilityOf(userProfile.notifyElement()));

        //wait.until(ExpectedConditions.elementToBeClickable(userProfile.notifyElement()));
        String msg_text = userProfile.notifyText();

        System.out.println("------------------------------------"+msg_text);

        if(!msg_text.equals("Successfully updated user")){
            throw new InterruptedException("Error while saving changes");
        }
	}

	@Step("Change Permission Groups") 
	public void changePermissionGroups() throws InterruptedException{
        userProfile.chooseSelect2Option("s2id_formPermissionGroups", "2");
    }
    
    @Step("Click on Password Tab")
	public void clickOnPasswordTab() throws InterruptedException{
       Driver.webDriver.get("https://integration.ps.anexia-it.com/user/b51ef959791941049d31867c6cc693cc/edit?tab=password");
    }

	@Step("Generate random password")
	public void generateRandomPassword() throws InterruptedException{
        String newGeneratedPassword = userProfile.generateRandomPassword();
        System.out.println("new password:" + newGeneratedPassword);
		
	}

	@Step("Enter new password")
	public void enterNewPassword() throws InterruptedException{
	  String newCreatedPassword = userProfile.createNewPassword("testUser09@2020");
		System.out.println("new password:" + newCreatedPassword);
	}

	@Step("Enter password confirmation")
	public void enterPasswordConfirmation() throws InterruptedException{
		userProfile.confirmPassword("testUser09@2020");
	}

	@Step("Click on API Tab")
	public void clickOnApiTab(){

		Driver.webDriver.get("https://integration.ps.anexia-it.com/user/b51ef959791941049d31867c6cc693cc/edit?tab=api");
	}

	@Step("Activate/Deactivate checkboxes")
	public void clickCheckboxes()throws InterruptedException{
		userProfile.checkboxAction();
	}

	
}
