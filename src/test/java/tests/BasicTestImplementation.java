package tests;

import java.util.List;

import com.thoughtworks.gauge.Gauge;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebElement;

import driver.Driver;

public class BasicTestImplementation {

    public  WebDriverWait wait = new WebDriverWait(Driver.webDriver, 30);


    public void selectOption(String id, String value) throws InterruptedException {

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='" + id + "']")));

        WebElement dropdown = Driver.webDriver.findElement(By.xpath("//*[@id='" + id + "']"));
        dropdown.click();

        if(value != ""){

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='select2-drop']/div[1]/input")));

            WebElement search = Driver.webDriver.findElement(By.xpath("//div[@id='select2-drop']/div[1]/input"));
            search.clear();
            search.click();
            search.sendKeys(value);

        }

        Thread.sleep(3000); //Wait until the search is finish - because otherwise it only finds the searching <li>-tag

        System.out.println(Driver.webDriver.findElement(By.xpath("//div[@id='select2-drop']//ul/li[1]")).getText());

        if(Driver.webDriver.findElement(By.xpath("//div[@id='select2-drop']//ul/li[1]")).getText().equals("No matches found")){
            Driver.webDriver.findElement(By.xpath("//*[@id='select2-drop-mask']")).click();
            throw new InterruptedException("No matches found");
        } else {
            Driver.webDriver.findElement(By.xpath("//div[@id='select2-drop']//ul/li[1]/div")).click();
            Gauge.writeMessage("Option: were choosed and clicked!");
            System.out.println("Option: were choosed and clicked!");
        }
    }

    public void clickButton(WebElement button) {
        button.click();
    }


    public void fillInputField(String value, WebElement inputField) {
        inputField.clear();
        inputField.click();
        inputField.sendKeys(value);
    }


   
    public void select2Option(String id, String value) throws InterruptedException {

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='" + id + "']")));

        WebElement dropdown = Driver.webDriver.findElement(By.xpath("//*[@id='" + id + "']"));
        dropdown.click();

        if(value != ""){

            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(" //*[@id='select2-drop']")));

            WebElement search = Driver.webDriver.findElement(By.xpath(" //*[@id='select2-drop']"));
            //search.clear();
            search.click();
            search.sendKeys(value);

        }

        Thread.sleep(3000); //Wait until the search is finish - because otherwise it only finds the searching <li>-tag

        System.out.println(Driver.webDriver.findElement(By.xpath("//div[@id='select2-drop']//ul/li[1]")).getText());

        if(Driver.webDriver.findElement(By.xpath("//div[@id='select2-drop']//ul/li[1]")).getText().equals("No matches found")){
            Driver.webDriver.findElement(By.xpath("//*[@id='select2-drop-mask']")).click();
            throw new InterruptedException("No matches found");
        } else {
            Driver.webDriver.findElement(By.xpath("//div[@id='select2-drop']//ul/li[1]/div")).click();
            Gauge.writeMessage("Option: were choosed and clicked!");
            System.out.println("Option: were choosed and clicked!");
        }
    }

    public void selectMultipleOption(String id, String number) throws InterruptedException {

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='" + id + "']")));

        WebElement dropdown = Driver.webDriver.findElement(By.xpath("//*[@id='" + id + "']"));

        for (int i = 1; i <= Integer.parseInt(number); i++){
            dropdown.click();

            Thread.sleep(3000); //Wait until the search is finish

            System.out.println(Driver.webDriver.findElement(By.xpath("//div[@id='select2-drop']//ul/li[" + i + "]")).getText());

            if(Driver.webDriver.findElement(By.xpath("//div[@id='select2-drop']//ul/li[1]")).getText().equals("No matches found")){
                Driver.webDriver.findElement(By.xpath("//*[@id='select2-drop-mask']")).click();
                throw new InterruptedException("No matches found");
            } else {
                Driver.webDriver.findElement(By.xpath("//div[@id='select2-drop']//ul/li[" + i + "]/div")).click();
                Gauge.writeMessage("Option: were choosed and clicked!");
                System.out.println("Option: were choosed and clicked!");
            }
        }
    }



  
    public void clearMultipleSelectAll(String id){
        List <WebElement> options = Driver.webDriver.findElements(By.xpath("//div[@id='" + id +"']//ul/li"));
        System.out.println("\nSize " +id + " : " + options.size());
        for (int i = 1; i <= options.size()-2; i++) {
            System.out.println("Index for " + id + " is " + i);
            WebElement entry = Driver.webDriver.findElement(By.xpath("//div[@id='" + id +"']//ul/li[1]/a"));
            entry.click();
            System.out.println("x were clicked for " + id);
        }
    }


    public void clickCheckbox (WebElement element){
         element.click();
         Gauge.writeMessage("checkbox is clicked");
    }
}