package tests.API;

import com.thoughtworks.gauge.Step;
import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;


public class CorpITServices {
    String BASE_URL = "https://integration.ps.anexia-it.com/api";

    @Step("Send GET request")
    public void sendGetRequest() throws InterruptedException,IOException {
        HttpClient client = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://integration.ps.anexia-it.com/api/brtest_corpit_services/v1/corporate_services.json?api_key=QW7kO9-rABKB7DMfo-vCtKAM5RNvdSq1"))
                .GET() // GET is default
                .build();

                HttpResponse<String> response = client.send(request,
                HttpResponse.BodyHandlers.ofString());

        System.out.println(response.body());
    }

    @Step("Create a new Corporate Service")
    public void sendPostRequest() throws InterruptedException,IOException {
        
        HttpClient client = HttpClient.newHttpClient();
       

        Map<Object, Object> data = new HashMap<>();
        data.put("service_name", "API_ACTest_2");
        data.put("service_state", "1");
        data.put("service_owner", "John Doe");
        data.put("responsible_department","IT");
        data.put("phone","+12222222222");

        HttpRequest request = HttpRequest.newBuilder()
        .POST(ofFormData(data))
        .uri(URI.create("https://httpbin.org/post"))
        .setHeader("User-Agent", "Java 11 HttpClient Bot") // add request header
        .header("Content-Type", "application/x-www-form-urlencoded")
        .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
          // print status code
          System.out.println(response.statusCode());

          // print response body
          System.out.println(response.body());
        
    }
    public static HttpRequest.BodyPublisher ofFormData(Map<Object, Object> data) {
        var builder = new StringBuilder();
        for (Map.Entry<Object, Object> entry : data.entrySet()) {
            if (builder.length() > 0) {
                builder.append("&");
            }
            builder.append(URLEncoder.encode(entry.getKey().toString(), StandardCharsets.UTF_8));
            builder.append("=");
            builder.append(URLEncoder.encode(entry.getValue().toString(), StandardCharsets.UTF_8));
        }
        return HttpRequest.BodyPublishers.ofString(builder.toString());
    }
        
}


//QW7kO9-rABKB7DMfo-vCtKAM5RNvdSq1
    
