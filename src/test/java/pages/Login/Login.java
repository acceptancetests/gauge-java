package pages.Login;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import pages.*;


public class Login extends BasePage{
  
    WebDriver driver;
    @FindBy(id="login-username") WebElement inp_username;

    @FindBy(id="login-submit") 
     private WebElement btn_continue;

    @FindBy(id="login-password") 
    private WebElement inp_password;
    
    @FindBy(xpath="/html/body/div[1]/div[1]/div/div/form/div[5]/div/div/div[1]/label/div") 
    private WebElement chb_ssignedIn;

    @FindBy(xpath="/html/body/div[6]/div") 
    private WebElement msg_wrongLoginValues;

    @FindBy(xpath="//*[@id='login-submit']") 
    private WebElement btn_submit;


    public Login(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
       
    public WebElement getUsernameInput()
    {
        return inp_username;
    }

    public void enterUsername(String userName){
        inp_username.sendKeys(userName);
    }

    public void enterPassword(String password){
        inp_password.sendKeys(password);
    }

    public void clickSubmitBtn(){
        btn_submit.click();
    }

    public void staysignedin(){
        chb_ssignedIn.click();
    }


}