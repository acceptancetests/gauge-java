// File:         [BasePage.java]
// Created:      [2020/08/14 creation date]
// Author:       <A HREF="mailto:[eoberrauner@anexia-it.com]">[Elena Oberrauner]</A>
// Repository:   https://bitbucket.org/acceptancetests/gauge-java/src/master/

package pages;

import com.thoughtworks.gauge.datastore.ScenarioDataStore;
import com.thoughtworks.gauge.datastore.SpecDataStore;
import com.thoughtworks.gauge.datastore.SuiteDataStore;

public abstract class BasePage {
    protected static String URL = System.getenv("APP_ENDPOINT");

    //to store values globally within scenario
    public void storeStringToScenarioDataStore(String key, String value) {
        
        ScenarioDataStore.put(key, value);
    }

    //to store values globally within specs
    public void storeStringToSpecDataStore(String key, String value) {

        SpecDataStore.put(key, value);
    }

    public void storeStringToSuiteDataStore(String key, String value) {
      
        SuiteDataStore.put(key, value);
    }

    public String fetchStringFromScenarioDataStore(String key) {
    
        return (String) ScenarioDataStore.get(key);
    }

    public String fetchStringFromSpecDataStore(String key) {
 
        return (String) SpecDataStore.get(key);
    }

    public String fetchStringFromSuiteDataStore(String key) {
       
        return (String) SuiteDataStore.get(key);
    }

}