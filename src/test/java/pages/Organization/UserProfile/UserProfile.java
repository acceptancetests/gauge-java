package pages.Organization.UserProfile;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.gauge.Gauge;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


import pages.BasePage;
import tests.BasicTestImplementation;

public class UserProfile extends BasePage {

    public BasicTestImplementation basicTestImplementation = new BasicTestImplementation();
  
    WebDriver driver;

    //WebDriverWait wait = new WebDriverWait(driver, 2);

    @FindBy(id="formFirstName") 
    WebElement inp_firstname;

    @FindBy(id="formLastName") 
     private WebElement inp_lastname;

    @FindBy(id="formZip") 
    private WebElement inp_zip;
    
    @FindBy(id="formCity") 
    private WebElement inp_city;

    @FindBy(id="formMobilePhone") 
    private WebElement inp_mobilePhone;

    @FindBy(xpath="/html/body/div[6]/div") 
    private WebElement s2_permissionGroups;

    @FindBy(id="submit-all-forms") 
    private WebElement btn_save;

    @FindBy(xpath="//*[@id='anx-modules']/div[2]/a") 
    private WebElement s_Organization;

    @FindBy(xpath="//*[@id='anx-modules']/div[2]/div/ul/li[2]") 
    private WebElement s_UserProfile;

    @FindBy(xpath="/html/body/div[2]/div") 
    private WebElement msg_notify;

    //****Password****
    @FindBy(xpath="//*[@id='password-24-group']/div/span/button[1]") 
    private WebElement btn_randomPassword;

    @FindBy(xpath="//*[@id='password-24-input']") 
    private WebElement inp_randomPassword;

    @FindBy(xpath="//*[@id='password-24-confirm-input']") 
    private WebElement inp_passwordConfirm;

    @FindBy(xpath="//*[@id='password-24-input']") 
    private WebElement inp_password;


    @FindBy(xpath="//*[@id='authenticationFormGroup']/div/div[2]/div") 
    private WebElement chb_TokenAuthentication;

    @FindBy(xpath="//*[@id='authenticationFormGroup']/div/div[3]/div") 
    private WebElement chb_HTTPSignatureAuthentication;

    

    public UserProfile(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
       
    public void enterFormInputs(String firstname, String lastname, String zip, String city, String mobilePhone){
        inp_firstname.clear();
        inp_firstname.sendKeys(firstname);
        
        inp_lastname.clear();
        inp_lastname.sendKeys(lastname);
        
        inp_zip.clear();
        inp_zip.sendKeys(zip);

        inp_city.clear();
        inp_city.sendKeys(city);

        inp_mobilePhone.clear();
        inp_mobilePhone.sendKeys(mobilePhone);
    }

    public void saveChanges(){
        btn_save.click();
        Gauge.writeMessage("Button was clicked");
    }

    public void chooseSelect2Option(String id, String number) throws InterruptedException {
        basicTestImplementation.clearMultipleSelectAll(id);
        basicTestImplementation.selectMultipleOption(id, number);
    }

    public void enterScopePage(){
        s_Organization.click();
        //wait.until(ExpectedConditions.visibilityOf(s_UserProfile));
        s_UserProfile.click();
    }

    public List<WebElement> returningElements(){
        
        List<WebElement> userProfileElements = new ArrayList<WebElement>();
        userProfileElements.add(s_Organization);
        userProfileElements.add(s_UserProfile);

        return userProfileElements;
    }

    public String notifyText(){
        String text = msg_notify.getText();
        return text;
    }

    public WebElement notifyElement(){
        return msg_notify;
    }

    public String generateRandomPassword() throws InterruptedException {
        btn_randomPassword.click();
       Thread.sleep(3000);
        return inp_randomPassword.getText();
    }

    public String createNewPassword(String password){
        inp_password.click();
        inp_password.clear();
        inp_password.sendKeys(password);

        String newCreatedPassword = inp_password.getText();
        return newCreatedPassword;
    }

    public void confirmPassword(String password){
        inp_passwordConfirm.click();
        inp_passwordConfirm.clear();
        inp_passwordConfirm.sendKeys(password);
    }


    public void checkboxAction() throws InterruptedException {
        basicTestImplementation.clickCheckbox(chb_TokenAuthentication);
        basicTestImplementation.clickCheckbox(chb_HTTPSignatureAuthentication);
    }
    
    
}
