# Login specification

## login with valid data
Tags: login valid
* Goto anexia internal-engine page
* Enter valid username
* Click on Continue
* Enter valid password
* Click on Login 
* Verify if login was successful

## login with valid data and activate checkbox 'Stay signed in'
Tags: login checkbox
* Goto anexia internal-engine page
* Enter valid username
* Click on Continue
* Enter valid password
* Activate checkbox to stay signed in
* Click on Login 
* Verify if login was successful 

## login with invalid username
Tags: logn invalid
* Goto anexia internal-engine page
* Enter invalid username
* Click on Continue
* Enter valid password
* Click on Login
* Verify text of errorMsg 
