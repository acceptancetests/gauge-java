# UserTeams specification

## User Logout
User has to be logged in

Tags: logout user
* Open integration page with stored cookies
* Click on username
* Click on logout
* Verify LogoutMsg

## Add new user to Organization
User has to be logged in
Tags: create new user
* Open integration page with stored cookies
* Click on Account within ANEXIA scope
* Click on Users&Teams
* Click on Add User
* Enter data within the modal

## Delete user 
User has to be logged in
* Open integration page with stored cookies
* Click on Account within ANEXIA scope
* Click on Users&Teams
* Search
* Verify ToolTip for DeleteBtn
* Delete User

## Edit User - assign to persmissuion group
* Open integration page with stored cookies
* Click on Account within ANEXIA scope
* Click on Users&Teams
* Search
* Assign new Permission group
* Click Save Button
* Verify editation 

## Remove User from team
* Open integration page with stored cookies
* Go to Teams
* Edit Team






