# UserProfile specification


## Change User Data
* Open integration page with stored cookies
* Click on UserProfile
* Change User Data
* Change Permission Groups
* Click Save Button
* Verify changes


## Change Password with generated random Password
* Open integration page with stored cookies
* Click on UserProfile
* Click on Password Tab
* Generate random password
* Click Save Button
* Verify changes

## Change Password by entering the password 
* Open integration page with stored cookies
* Click on UserProfile
* Click on Password Tab
* Enter new password
* Enter password confirmation
* Click Save Button
* Verify changes

## Activate API authentification methods
* Open integration page with stored cookies
* Click on UserProfile
* Click on API Tab
* Activate/Deactivate checkboxes
* Click Save Button
* Verify changes


